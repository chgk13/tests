import time
from getpass import getpass
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys


class Autoliker():

    def __init__(self):
        self.driver = webdriver.Firefox()

    def login(self):
        self.driver.get('https://www.tinder.com/')
        time.sleep(10)

        by_phone = self.driver.find_element_by_xpath(
            '/html/body/div[2]/div/div/div[2]/div/div[3]/div[2]/button')
        by_phone.click()
        phone = input('Enter your phone number: ')
        phone_input = self.driver.find_element_by_name('phone_number')
        phone_input.send_keys(phone)
        cont_button = self.driver.find_element_by_xpath(
            '/html/body/div[2]/div/div/div[2]/button')
        cont_button.click()
        time.sleep(2)

        code = input('Enter your verification code: ')
        for i, char in enumerate(code):
            code_input = self.driver.find_element_by_xpath(
                f'/html/body/div[2]/div/div/div[2]/div[3]/input[{i+1}]')
            code_input.send_keys(char)
        cont_button = self.driver.find_element_by_xpath(
            '/html/body/div[2]/div/div/div[2]/button')
        cont_button.click()
        time.sleep(2)

        next_btn = self.driver.find_element_by_xpath(
            '/html/body/div[1]/div/span/div/div[2]/div/div[1]/div[1]/div/div[3]/button')
        next_btn.click()
        time.sleep(2)
        next_btn = self.driver.find_element_by_xpath(
            '/html/body/div[1]/div/span/div/div[2]/div/div/main/div/div[3]/button')
        next_btn.click()
        time.sleep(2)
        allow_track = self.driver.find_element_by_xpath(
            '/html/body/div[1]/div/span/div/div[2]/div/div/div[1]/div/div[3]/button[1]')
        allow_track.click()
        time.sleep(2)
        notifications = self.driver.find_element_by_xpath(
            '/html/body/div[1]/div/span/div/div[2]/div/div/div[1]/div/div[3]/button[1]')
        notifications.click()
        time.sleep(2)
        print('You are in.')
        return True

    def autolike(self):
        like_btn = self.driver.find_element_by_xpath(
            '/html/body/div[1]/div/span/div/div[1]/div/main/div[1]/div/div/div[1]/div/div[2]/button[4]')
        time.sleep(5)
        try:
            while self.driver.find_element_by_xpath("/html/body/div[1]/div/span/div/div[1]/div/main/div[1]/div/div/div[1]/div/div[2]/button[4]"):
                like_btn.click()
                time.sleep(2)
        except Exception:
            print("No more likes for today. Thay want money.")
            input('Press enter to quit.')
        finally:
            self.driver.quit()


if __name__ == '__main__':
    tinder = Autoliker()
    tinder.login()
    ok = input(
        '''Type yes, if you are in like page, or do all login stuff for me 
(share lokation and enable notification) and than type yes: ''')
    if ok:
        tinder.autolike()
